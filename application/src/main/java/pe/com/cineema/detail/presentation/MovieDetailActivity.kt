package pe.com.cineema.detail.presentation

import android.util.SparseArray
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.util.forEach
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import pe.com.cineema.R
import pe.com.cineema.application.common.ui.SPACE
import pe.com.cineema.application.common.util.IMAGE_URL_BASE
import pe.com.cineema.application.common.util.ONE
import pe.com.cineema.application.common.util.TWO
import pe.com.cineema.application.common.util.YOUTUBE_URL_BASE
import pe.com.cineema.base.component.generic.Resource
import pe.com.cineema.base.component.generic.Status
import pe.com.cineema.base.component.ui.BaseActivity
import pe.com.cineema.databinding.ActivityMovieDetailBinding
import pe.com.cineema.detail.domain.model.MovieDetailModel
import pe.com.cineema.detail.domain.response.VideoResultResponse

@AndroidEntryPoint
class MovieDetailActivity : BaseActivity<ActivityMovieDetailBinding>() {

    private var movieId: Int = 0
    private val movieDetailViewModel: MovieDetailViewModel by viewModels()
    private val exoPlayer: ExoPlayer by lazy {
        ExoPlayer.Builder(baseContext).build()
    }

    companion object {
        const val MOVIE_ID_KEY = "id"
    }

    override fun getViewBinding() = ActivityMovieDetailBinding.inflate(layoutInflater)

    override fun initConfig() {
        setupViews()
        setupBundle()
        setupObservers()
    }

    private fun setupViews() {
        binding.finishImageView.setOnClickListener {
            finish()
        }
    }

    private fun setupBundle() {
        val extras = intent.extras ?: return
        movieId = Integer.parseInt(extras.get(MOVIE_ID_KEY).toString())
    }

    private fun setupObservers() {
        val movieDetail = movieDetailViewModel.getMovieDetail(movieId)
        movieDetail.observe(this, { detailModel ->
            detailModel?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        showMovieDetail(resource)
                    }
                    Status.ERROR -> {
                        showError(resource)
                    }
                    Status.LOADING -> {
                        showLoading()
                    }
                }
            }
        })
    }

    private fun showMovieDetail(resource: Resource<MovieDetailModel>) {
        hideLoading()
        setupPoster(resource)
        setupOverview(resource)
        setupVoteAverage(resource)
        getVideos()
    }

    private fun setupPoster(resource: Resource<MovieDetailModel>) {
        val detailModel = resource.data
        val url = IMAGE_URL_BASE.plus(detailModel?.posterPath)
        Picasso.get().load(url)
            .placeholder(R.drawable.movie_placeholder)
            .error(R.drawable.movie_placeholder)
            .into(binding.posterImageView)
    }

    private fun setupOverview(resource: Resource<MovieDetailModel>) {
        val detailModel = resource.data
        binding.overviewTextView.text = detailModel?.overview
    }

    private fun setupVoteAverage(resource: Resource<MovieDetailModel>) {
        val detailModel = resource.data
        val voteAverage = detailModel?.voteAverage
        val voteAverageLabel = SPACE + getString(R.string.vote_average)
        val descriptionVoteAverage = (voteAverage).toString().plus(voteAverageLabel)
        binding.detailExtendedFloatingActionButton.text = descriptionVoteAverage
    }

    private fun getVideos() {
        movieDetailViewModel.getVideos(movieId).observe(this, { videoResultResponse ->
            videoResultResponse?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        showVideos(resource)
                    }
                    Status.ERROR -> {
                        showVideoError(resource)
                    }
                    Status.LOADING -> {
                        showVideoLoading()
                    }
                }
            }
        })
    }

    private fun showVideoLoading() {
        binding.videoStyledPlayerView.visibility = View.VISIBLE
    }

    private fun hideVideoLoading() {
        binding.videoStyledPlayerView.visibility = View.GONE
    }

    private fun showVideos(resource: Resource<VideoResultResponse>) {
        val results = resource.data?.results ?: emptyList()
        val key = results[0].key
        binding.videoStyledPlayerView.player = exoPlayer
        setupYouTubeExtractor(key)

    }

    private fun setupYouTubeExtractor(key: String?) {
        object : YouTubeExtractor(baseContext) {
            override fun onExtractionComplete(ytFiles: SparseArray<YtFile>?, vMeta: VideoMeta?) {
                if (ytFiles == null) return
                setupMediaSource(ytFiles)
            }
        }.extract(YOUTUBE_URL_BASE.plus(key))
    }

    private fun SparseArray<YtFile>.getTags(): MutableList<Int> {
        val tags = mutableListOf<Int>()
        this.forEach { key, _ -> tags.add(key) }
        return tags
    }

    private fun setupMediaSource(ytFiles: SparseArray<YtFile>) {
        val tags = ytFiles.getTags()
        if (tags.size < TWO) return

        val audioTag = ytFiles[tags[tags.size - ONE]]
        val videoTag = ytFiles[tags[tags.size - TWO]]
        val audioMediaSource = getMediaSource(audioTag.url)
        val videoMediaSource = getMediaSource(videoTag.url)

        val mergingMediaSource =
            MergingMediaSource(true, videoMediaSource, audioMediaSource)
        exoPlayer.setMediaSource(mergingMediaSource, true)
        exoPlayer.prepare()
        exoPlayer.playWhenReady = true
    }

    private fun getMediaSource(url: String): MediaSource {
        return ProgressiveMediaSource
            .Factory(DefaultHttpDataSource.Factory())
            .createMediaSource(MediaItem.fromUri(url))
    }

    private fun showError(resource: Resource<MovieDetailModel>) {
        Toast.makeText(baseContext, resource.message, Toast.LENGTH_SHORT).show()
    }

    private fun showVideoError(resource: Resource<VideoResultResponse>) {
        Toast.makeText(baseContext, resource.message, Toast.LENGTH_SHORT).show()
    }

    private fun showLoading() {
        binding.detailProgressBar.visibility = View.VISIBLE
        binding.posterImageView.visibility = View.GONE
        binding.overviewTextView.visibility = View.GONE
        binding.detailExtendedFloatingActionButton.visibility = View.GONE
        binding.videoStyledPlayerView.visibility = View.GONE
    }

    private fun hideLoading() {
        binding.detailProgressBar.visibility = View.GONE
        binding.posterImageView.visibility = View.VISIBLE
        binding.overviewTextView.visibility = View.VISIBLE
        binding.detailExtendedFloatingActionButton.visibility = View.VISIBLE
    }

    override fun onStop() {
        super.onStop()
        binding.videoStyledPlayerView.player?.stop()
    }
}