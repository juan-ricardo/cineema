package pe.com.cineema.detail.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import pe.com.cineema.application.common.ui.GET_DETAIL_EXCEPTION
import pe.com.cineema.application.common.ui.GET_VIDEOS_EXCEPTION
import pe.com.cineema.base.component.generic.Resource
import pe.com.cineema.detail.datasource.network.MovieDetailNetworkDataRepository
import pe.com.cineema.detail.datasource.network.VideoNetworkDataRepository
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val detailNetowrkDataRepository: MovieDetailNetworkDataRepository,
    private val videoNetowrkDataRepository: VideoNetworkDataRepository
) : ViewModel() {

    fun getMovieDetail(id: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = detailNetowrkDataRepository.getMovieDetail(id)))
        } catch (exception: Exception) {
            val error = exception.message ?: GET_DETAIL_EXCEPTION
            emit(Resource.error(data = null, message = error))
        }
    }

    fun getVideos(id: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = videoNetowrkDataRepository.getVideos(id)))
        } catch (exception: Exception) {
            val error = exception.message ?: GET_VIDEOS_EXCEPTION
            emit(Resource.error(data = null, message = error))
        }
    }
}