package pe.com.cineema.detail.domain.model

import pe.com.cineema.base.component.util.EMPTY

class MovieDetailModel(
    val adult: Boolean = false,
    val backdropPath: String = EMPTY,
    val budget: Long = 0,
    val homepage: String = EMPTY,
    val id: Int = 0,
    val originalLanguage: String = EMPTY,
    val originalTitle: String = EMPTY,
    val overview: String = EMPTY,
    val popularity: Float = 0.00f,
    val posterPath: String = EMPTY,
    val releaseDate: String = EMPTY,
    val revenue: Float = 0.00f,
    val runtime: Float = 0.00f,
    val status: String = EMPTY,
    val tagline: String = EMPTY,
    val title: String = EMPTY,
    val video: Boolean = false,
    val voteAverage: Float = 0.00f,
    val voteCount: Int = 0
)