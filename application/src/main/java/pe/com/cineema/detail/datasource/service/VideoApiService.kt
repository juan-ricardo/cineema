package pe.com.cineema.detail.datasource.service

import pe.com.cineema.detail.domain.response.VideoResultResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface VideoApiService {
    @GET("movie/{id}/videos")
    suspend fun getVideosResultResponse(@Path("id") id: Int): VideoResultResponse
}