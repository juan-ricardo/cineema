package pe.com.cineema.detail.datasource.network

import pe.com.cineema.detail.datasource.service.MovieDetailApiService
import pe.com.cineema.detail.domain.mapper.MovieDetailMapper
import pe.com.cineema.detail.domain.model.MovieDetailModel
import pe.com.cineema.detail.domain.repository.MovieDetailRepository
import javax.inject.Inject

class MovieDetailNetworkDataRepository @Inject constructor(
    private val movieDetailApiService: MovieDetailApiService,
    private val movieDetailMapper: MovieDetailMapper
) : MovieDetailRepository {
    override suspend fun getMovieDetail(id: Int): MovieDetailModel {
        val response = movieDetailApiService.getMovieDetailResponse(id)
        return movieDetailMapper.transform(response)
    }
}