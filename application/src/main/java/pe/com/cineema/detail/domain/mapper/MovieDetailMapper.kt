package pe.com.cineema.detail.domain.mapper

import pe.com.cineema.base.component.util.EMPTY
import pe.com.cineema.detail.domain.model.MovieDetailModel
import pe.com.cineema.detail.domain.response.MovieDetailResponse
import javax.inject.Inject

class MovieDetailMapper @Inject constructor() {

    fun transform(response: MovieDetailResponse): MovieDetailModel {

        val adult = response.adult ?: false
        val backdropPath = response.backdropPath ?: EMPTY
        val budget = response.budget ?: 0
        val homepage = response.homepage ?: EMPTY
        val id = response.id ?: 0
        val originalLanguage = response.originalLanguage ?: EMPTY
        val originalTitle = response.originalTitle ?: EMPTY
        val overview = response.overview ?: EMPTY
        val popularity = response.popularity ?: 0.00f
        val posterPath = response.posterPath ?: EMPTY
        val releaseDate = response.releaseDate ?: EMPTY
        val revenue = response.revenue ?: 0.00f
        val runtime = response.runtime ?: 0.00f
        val status = response.status ?: EMPTY
        val tagline = response.tagline ?: EMPTY
        val title = response.title ?: EMPTY
        val video = response.video ?: false
        val voteAverage = response.voteAverage ?: 0.00f
        val voteCount = response.voteCount ?: 0

        return MovieDetailModel(
            adult = adult,
            backdropPath = backdropPath,
            budget = budget,
            homepage = homepage,
            id = id,
            originalLanguage = originalLanguage,
            originalTitle = originalTitle,
            overview = overview,
            popularity = popularity,
            posterPath = posterPath,
            releaseDate = releaseDate,
            revenue = revenue,
            runtime = runtime,
            status = status,
            tagline = tagline,
            title = title,
            video = video,
            voteAverage = voteAverage,
            voteCount = voteCount
        )
    }
}