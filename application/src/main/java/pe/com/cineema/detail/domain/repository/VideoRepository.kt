package pe.com.cineema.detail.domain.repository

import pe.com.cineema.detail.domain.response.VideoResultResponse

interface VideoRepository {
    suspend fun getVideos(id: Int): VideoResultResponse
}