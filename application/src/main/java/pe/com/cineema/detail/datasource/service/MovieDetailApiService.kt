package pe.com.cineema.detail.datasource.service

import pe.com.cineema.detail.domain.response.MovieDetailResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieDetailApiService {
    @GET("movie/{id}")
    suspend fun getMovieDetailResponse(@Path("id") id: Int): MovieDetailResponse
}