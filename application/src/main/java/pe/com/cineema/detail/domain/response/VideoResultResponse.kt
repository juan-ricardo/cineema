package pe.com.cineema.detail.domain.response

import com.google.gson.annotations.SerializedName

class VideoResultResponse {
    @SerializedName("id")
    val id: Int? = null

    @SerializedName("results")
    val results: List<VideoResponse>? = null
}