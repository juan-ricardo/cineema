package pe.com.cineema.detail.datasource.network

import pe.com.cineema.detail.datasource.service.VideoApiService
import pe.com.cineema.detail.domain.repository.VideoRepository
import pe.com.cineema.detail.domain.response.VideoResultResponse
import javax.inject.Inject

class VideoNetworkDataRepository @Inject constructor(
    private val videoApiService: VideoApiService
) : VideoRepository {

    override suspend fun getVideos(id: Int): VideoResultResponse {
        return videoApiService.getVideosResultResponse(id)
    }
}