package pe.com.cineema.detail.domain.response

import com.google.gson.annotations.SerializedName

class VideoResponse(
    @SerializedName("name")
    val name: String? = null,

    @SerializedName("key")
    val key: String? = null,

    @SerializedName("site")
    val site: String? = null,

    @SerializedName("size")
    val size: String? = null,

    @SerializedName("type")
    val type: String? = null,

    @SerializedName("id")
    val id: String? = null,
)