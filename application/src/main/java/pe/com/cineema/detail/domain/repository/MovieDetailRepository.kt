package pe.com.cineema.detail.domain.repository

import pe.com.cineema.detail.domain.model.MovieDetailModel

interface MovieDetailRepository {
    suspend fun getMovieDetail(id: Int): MovieDetailModel
}