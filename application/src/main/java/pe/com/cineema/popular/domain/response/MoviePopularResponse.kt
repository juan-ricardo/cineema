package pe.com.cineema.popular.domain.response

import com.google.gson.annotations.SerializedName
import pe.com.cineema.dashboard.domain.response.ResultResponse

class MoviePopularResponse(
    @SerializedName("page")
    val page: Int? = null,

    @SerializedName("results")
    val results: List<ResultResponse>? = null,

    @SerializedName("total_pages")
    val totalPages: Int? = null,

    @SerializedName("total_results")
    val totalResults: Int? = null
)