package pe.com.cineema.popular.dependence.injection

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pe.com.cineema.application.network.NetworkHandler
import pe.com.cineema.popular.datasource.MoviePopularDataSourceRepository
import pe.com.cineema.popular.datasource.database.MoviePopularDatabaseDataRepository
import pe.com.cineema.popular.datasource.database.room.config.RoomDatabaseManager
import pe.com.cineema.popular.datasource.network.MoviePopularNetworkDataRepository
import pe.com.cineema.popular.datasource.network.service.MoviePopularApiService
import pe.com.cineema.popular.domain.mapper.MoviePopularMapper
import pe.com.cineema.popular.domain.repository.MoviePopularRepository
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MoviePopularModule {

    @Singleton
    @Provides
    fun popularApiServiceProvide(retrofit: Retrofit): MoviePopularApiService {
        return retrofit.create(MoviePopularApiService::class.java)
    }

    @Singleton
    @Provides
    fun popularNetowrkDataRepositoryProvide(
        popularApiService: MoviePopularApiService,
        popularMapper: MoviePopularMapper
    ): MoviePopularNetworkDataRepository {
        return MoviePopularNetworkDataRepository(popularApiService, popularMapper)
    }

    @Singleton
    @Provides
    fun roomDatabaseManagerProvide(
        application: Application
    ): RoomDatabaseManager {
        return RoomDatabaseManager.getDatabase(application.applicationContext)
    }

    @Singleton
    @Provides
    fun popularDatabaseDataRepositoryProvide(
        roomDatabaseManager: RoomDatabaseManager,
        popularMapper: MoviePopularMapper
    ): MoviePopularDatabaseDataRepository {
        return MoviePopularDatabaseDataRepository(roomDatabaseManager, popularMapper)
    }

    @Singleton
    @Provides
    fun popularRepositoryProvide(
        popularNetowrkDataRepository: MoviePopularNetworkDataRepository,
        popularDatabaseDataRepository: MoviePopularDatabaseDataRepository,
        networkHandler: NetworkHandler
    ): MoviePopularRepository {
        return MoviePopularDataSourceRepository(
            popularNetowrkDataRepository,
            popularDatabaseDataRepository,
            networkHandler
        )
    }
}