package pe.com.cineema.popular.datasource.database.room.dao

import androidx.room.*
import pe.com.cineema.popular.domain.entity.MoviePopularEntity

@Dao
interface MoviePopularDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(moviePopularEntity: MoviePopularEntity)

    //@Insert
    //suspend fun insertAll(moviePopularEntities: List<MoviePopularEntity>)

    @Query("SELECT * FROM movie_popular_table")
    fun getAll(): List<MoviePopularEntity>

    @Query("SELECT * FROM movie_popular_table WHERE title LIKE :title")
    fun findByTitle(title: String): MoviePopularEntity

    @Update
    fun updateAll(vararg moviePopularEntities: MoviePopularEntity)

    @Delete
    fun delete(moviePopularEntity: MoviePopularEntity)

    //@Query("DELETE FROM movie_popular_table")
    //suspend fun deleteAll()
}