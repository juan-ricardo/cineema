package pe.com.cineema.popular.datasource.network

import pe.com.cineema.popular.datasource.network.service.MoviePopularApiService
import pe.com.cineema.popular.domain.mapper.MoviePopularMapper
import pe.com.cineema.popular.domain.model.MoviePopularModel
import javax.inject.Inject

class MoviePopularNetworkDataRepository @Inject constructor(
    private val popularApiService: MoviePopularApiService,
    private val popularMapper: MoviePopularMapper
) {
    suspend fun getPopularModels(): List<MoviePopularModel> {
        val popularResponse = popularApiService.getPopularResponse()
        return popularMapper.transform(popularResponse)
    }
}