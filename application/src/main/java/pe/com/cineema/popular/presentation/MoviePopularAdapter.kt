package pe.com.cineema.popular.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import pe.com.cineema.R
import pe.com.cineema.application.common.util.IMAGE_URL_BASE
import pe.com.cineema.application.common.listener.ItemOnClickListener
import pe.com.cineema.databinding.PopularCardItemBinding
import pe.com.cineema.popular.domain.model.MoviePopularModel

class MoviePopularAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var moviePopularModels = mutableListOf<MoviePopularModel>()
    private var filterMoviePopularModels = mutableListOf<MoviePopularModel>()
    private var itemOnClickListener: ItemOnClickListener? = null

    fun setItems(popularModels: MutableList<MoviePopularModel>) {
        this.moviePopularModels.addAll(popularModels)
        this.filterMoviePopularModels.addAll(popularModels)
    }

    fun setItemOnClickListener(itemOnClickListener: ItemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val popularCardItemBinding = PopularCardItemBinding.inflate(layoutInflater)
        return PopularViewHolder(popularCardItemBinding, itemOnClickListener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = moviePopularModels[position]
        if (holder is PopularViewHolder) {
            holder.onBind(item)
        }
    }

    inner class PopularViewHolder(
        private val popularCardItemBinding: PopularCardItemBinding,
        private val itemOnClickListener: ItemOnClickListener?
    ) : RecyclerView.ViewHolder(popularCardItemBinding.root) {
        fun onBind(moviePopularModel: MoviePopularModel) {
            setupItemOnClickListener(moviePopularModel)
            setupTitle(moviePopularModel)
            setupPoster(moviePopularModel)
        }

        private fun setupItemOnClickListener(moviePopularModel: MoviePopularModel) {
            popularCardItemBinding.root.setOnClickListener {
                itemOnClickListener?.onClick(
                    moviePopularModel,
                    popularCardItemBinding.posterImageView
                )
            }
        }

        private fun setupTitle(moviePopularModel: MoviePopularModel) {
            popularCardItemBinding.titleTextView.text = moviePopularModel.title
        }

        private fun setupPoster(popularModel: MoviePopularModel) {
            val url = IMAGE_URL_BASE.plus(popularModel.posterPath)
            val imageView = popularCardItemBinding.posterImageView
            loadImage(url, imageView)
        }

        private fun loadImage(url: String, imageView: ImageView) {
            Picasso.get()
                .load(url)
                .placeholder(R.drawable.movie_placeholder)
                .error(R.drawable.movie_placeholder)
                .into(imageView)
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val filterResults = FilterResults()
                if (charSequence.isNotEmpty()) {
                    val query = charSequence.toString()
                    val filters: MutableList<MoviePopularModel> = mutableListOf()
                    for (item in filterMoviePopularModels) {
                        val title = item.title?.contains(query, true)
                        val originalTitle = item.originalTitle?.contains(query, true)
                        if (title == true || originalTitle == true) {
                            filters.add(item)
                        }
                    }
                    filterResults.count = filters.size
                    filterResults.values = filters
                } else {
                    filterResults.count = filterMoviePopularModels.size
                    filterResults.values = filterMoviePopularModels
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                moviePopularModels = results.values as MutableList<MoviePopularModel>
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount() = moviePopularModels.size
}