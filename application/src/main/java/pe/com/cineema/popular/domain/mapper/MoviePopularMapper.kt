package pe.com.cineema.popular.domain.mapper

import pe.com.cineema.popular.domain.model.MoviePopularModel
import pe.com.cineema.popular.domain.response.MoviePopularResponse
import pe.com.cineema.dashboard.domain.response.ResultResponse
import pe.com.cineema.popular.domain.entity.MoviePopularEntity
import javax.inject.Inject

class MoviePopularMapper @Inject constructor() {

    fun transform(popularResponse: MoviePopularResponse?): List<MoviePopularModel> {
        val results = popularResponse?.results
        val popularModels = results?.map { resultResponse ->
            transformResponseToModel(resultResponse)
        }
        return popularModels ?: emptyList()
    }

    private fun transformResponseToModel(resultResponse: ResultResponse): MoviePopularModel {
        val adult = resultResponse.adult
        val backdropPath = resultResponse.backdropPath
        val id = resultResponse.id
        val originalLanguage = resultResponse.originalLanguage
        val originalTitle = resultResponse.originalTitle
        val overview = resultResponse.overview
        val posterPath = resultResponse.posterPath
        val releaseDate = resultResponse.releaseDate
        val title = resultResponse.title
        val video = resultResponse.video
        val voteAverage = resultResponse.voteAverage
        val voteCount = resultResponse.voteCount

        return MoviePopularModel(
            adult = adult,
            backdropPath = backdropPath,
            id = id,
            originalLanguage = originalLanguage,
            originalTitle = originalTitle,
            overview = overview,
            posterPath = posterPath,
            releaseDate = releaseDate,
            title = title,
            video = video,
            voteAverage = voteAverage,
            voteCount = voteCount
        )
    }

    fun transform(moviePopularEntities: List<MoviePopularEntity>?): List<MoviePopularModel> {
        val popularModels = moviePopularEntities?.map { moviePopularEntity ->
            transformResponseToModel(moviePopularEntity)
        }
        return popularModels ?: emptyList()
    }

    private fun transformResponseToModel(moviePopularEntity: MoviePopularEntity): MoviePopularModel {
        val adult = moviePopularEntity.adult
        val backdropPath = moviePopularEntity.backdropPath
        val id = moviePopularEntity.id
        val originalLanguage = moviePopularEntity.originalLanguage
        val originalTitle = moviePopularEntity.originalTitle
        val overview = moviePopularEntity.overview
        val posterPath = moviePopularEntity.posterPath
        val releaseDate = moviePopularEntity.releaseDate
        val title = moviePopularEntity.title
        val video = moviePopularEntity.video
        val voteAverage = moviePopularEntity.voteAverage
        val voteCount = moviePopularEntity.voteCount

        return MoviePopularModel(
            adult = adult,
            backdropPath = backdropPath,
            id = id,
            originalLanguage = originalLanguage,
            originalTitle = originalTitle,
            overview = overview,
            posterPath = posterPath,
            releaseDate = releaseDate,
            title = title,
            video = video,
            voteAverage = voteAverage,
            voteCount = voteCount
        )
    }

    fun transform(popularModel: MoviePopularModel): MoviePopularEntity {
        val adult = popularModel.adult
        val backdropPath = popularModel.backdropPath
        val id = popularModel.id
        val originalLanguage = popularModel.originalLanguage
        val originalTitle = popularModel.originalTitle
        val overview = popularModel.overview
        val posterPath = popularModel.posterPath
        val releaseDate = popularModel.releaseDate
        val title = popularModel.title
        val video = popularModel.video
        val voteAverage = popularModel.voteAverage
        val voteCount = popularModel.voteCount

        return MoviePopularEntity(
            adult = adult,
            backdropPath = backdropPath,
            id = id,
            originalLanguage = originalLanguage,
            originalTitle = originalTitle,
            overview = overview,
            posterPath = posterPath,
            releaseDate = releaseDate,
            title = title,
            video = video,
            voteAverage = voteAverage,
            voteCount = voteCount
        )
    }
}

