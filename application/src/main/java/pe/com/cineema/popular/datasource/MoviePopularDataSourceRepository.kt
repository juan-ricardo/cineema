package pe.com.cineema.popular.datasource

import pe.com.cineema.application.network.NetworkHandler
import pe.com.cineema.popular.datasource.database.MoviePopularDatabaseDataRepository
import pe.com.cineema.popular.datasource.network.MoviePopularNetworkDataRepository
import pe.com.cineema.popular.domain.model.MoviePopularModel
import pe.com.cineema.popular.domain.repository.MoviePopularRepository
import javax.inject.Inject
import kotlin.random.Random
import kotlin.random.nextInt

class MoviePopularDataSourceRepository @Inject constructor(
    private val moviePopularNetworkDataRepository: MoviePopularNetworkDataRepository,
    private val moviePopularDatabaseDataRepository: MoviePopularDatabaseDataRepository,
    private val networkHandler: NetworkHandler
) : MoviePopularRepository {

    /***
     * Para obtener las películas populares, primero consultamos la base de datos (local).
     * Si está vacía, consultamos la API Rest (themoviedb). Por último, sincronizamos
     * las películas populares en la base de datos (local).
     */
    override suspend fun getMoviePopularModels(): List<MoviePopularModel> {
        val isNetworkAvailable = networkHandler.isNetworkAvailable()
        return if (isNetworkAvailable) {
            setupNetworkAvailable()
        } else {
            getMoviePopularModelsFromDatabase()
        }
    }
    /***
     * Obtiene las pelculas polares de la base de datos o del API Rest.
     * De acuerdo al filtro de sincronización.
     */
    private suspend fun setupNetworkAvailable(): List<MoviePopularModel> {
        val moviePopularModels = moviePopularNetworkDataRepository.getPopularModels()
        return if (synchroniseWithDatabase()) {
            saveMoviePopularModelsInDatabase(moviePopularModels)
            getMoviePopularModelsFromDatabase()
        } else {
            moviePopularModels
        }
    }

    /***
     * Guarda las pelculas polares en la base de datos.
     */
    private fun saveMoviePopularModelsInDatabase(moviePopularModels: List<MoviePopularModel>) {
        moviePopularDatabaseDataRepository.savePopularModels(moviePopularModels)
    }

    /***
     * Obtiene las pelculas polares de la base de datos.
     */
    private fun getMoviePopularModelsFromDatabase(): List<MoviePopularModel> {
        return moviePopularDatabaseDataRepository.getPopularModels()
    }

    /***
     * Por ahora estoy considerando este filtro para la sincronización local con la red.
     * Ahi se puede configurar casos, como por ejemplo puede ser una determinada hora,
     * o que el API Rest nos mande algun flag, etc.
     */
    private fun synchroniseWithDatabase(): Boolean {
        val value = Random.nextInt(1..12)
        return value % 2 == 0
    }
}