package pe.com.cineema.popular.datasource.network.service

import pe.com.cineema.popular.domain.response.MoviePopularResponse
import retrofit2.http.GET

interface MoviePopularApiService {
    @GET("movie/popular")
    suspend fun getPopularResponse(): MoviePopularResponse
}