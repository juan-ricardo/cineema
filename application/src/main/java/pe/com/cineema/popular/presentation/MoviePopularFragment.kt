package pe.com.cineema.popular.presentation

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.ConsoleMessage
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.com.cineema.R
import pe.com.cineema.application.common.listener.ItemOnClickListener
import pe.com.cineema.application.common.ui.SpacesItemDecoration
import pe.com.cineema.application.common.util.TWO
import pe.com.cineema.base.component.generic.Resource
import pe.com.cineema.base.component.generic.Status
import pe.com.cineema.base.component.ui.BaseFragment
import pe.com.cineema.databinding.FragmentMoviePopularBinding
import pe.com.cineema.detail.presentation.MovieDetailActivity
import pe.com.cineema.popular.domain.model.MoviePopularModel

@AndroidEntryPoint
class MoviePopularFragment : BaseFragment<FragmentMoviePopularBinding>(), ItemOnClickListener {

    private val moviePopularViewModel: MoviePopularViewModel by viewModels()
    private val moviePopularAdapter: MoviePopularAdapter by lazy { MoviePopularAdapter() }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToRoot: Boolean?
    ): FragmentMoviePopularBinding {
        return FragmentMoviePopularBinding.inflate(layoutInflater)
    }

    override fun initConfig() {
        setupViews()
        setupObservers()
    }

    private fun setupViews() {
        setupMoviePopularRecyclerView()
        setupErrorOccurred()
    }

    private fun setupMoviePopularRecyclerView() {
        val gridLayoutManager = GridLayoutManager(context, TWO)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.cineema_margin_4)
        val spacesItemDecoration = SpacesItemDecoration(spacingInPixels)
        binding.popularRecyclerView.layoutManager = gridLayoutManager
        binding.popularRecyclerView.addItemDecoration(spacesItemDecoration)
    }

    private fun setupObservers() {
        getMoviePopularModels()
    }

    private fun getMoviePopularModels() {
        val moviePopularModels = moviePopularViewModel.getMoviePopularModels()
        moviePopularModels.observe(this, { values ->
            values?.let { resource -> statusForMoviePopularModels(resource) }
        })
    }

    private fun statusForMoviePopularModels(resource: Resource<List<MoviePopularModel>>) {
        when (resource.status) {
            Status.SUCCESS -> { showMoviePopularModels(resource) }
            Status.ERROR -> { showError() }
            Status.LOADING -> { showProgressBar() }
        }
    }

    private fun showMoviePopularModels(resource: Resource<List<MoviePopularModel>>) {
        val moviePopulars = getMoviePopulars(resource)
        if (moviePopulars.isEmpty()) {
            showErrorOccurred()
        } else {
            setupMoviePopularAdapter(moviePopulars)
            showMoviePopularRecyclerView()
        }
        hideProgressBar()
    }

    private fun setupMoviePopularAdapter(moviePopulars: MutableList<MoviePopularModel> = mutableListOf()) {
        moviePopularAdapter.setItems(moviePopulars)
        moviePopularAdapter.setItemOnClickListener(this)
        binding.popularRecyclerView.adapter = moviePopularAdapter
    }

    fun filterPopularMovies(query: String) {
        moviePopularAdapter.filter.filter(query)
    }

    private fun getMoviePopulars(resource: Resource<List<MoviePopularModel>>): MutableList<MoviePopularModel> {
        return resource.data?.toMutableList() ?: mutableListOf()
    }

    private fun showError() {
        retryErrorOccurred()
        hideMoviePopularRecyclerView()
        hideProgressBar()
    }

    private fun setupErrorOccurred() {
        binding.errorOccurred.retryMaterialButton.setOnClickListener { retryErrorOccurred() }
    }

    private fun retryErrorOccurred() {
        hideErrorOccurred()
        showProgressBar()
        getMoviePopularModels()
    }

    private fun showProgressBar() {
        binding.popularProgressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.popularProgressBar.visibility = View.GONE
    }

    private fun showMoviePopularRecyclerView() {
        binding.popularRecyclerView.visibility = View.VISIBLE
    }

    private fun hideMoviePopularRecyclerView() {
        binding.popularRecyclerView.visibility = View.GONE
    }

    private fun showErrorOccurred() {
        binding.errorOccurred.root.visibility = View.VISIBLE
    }

    private fun hideErrorOccurred() {
        binding.errorOccurred.root.visibility = View.GONE
    }

    companion object {
        fun newInstance() = MoviePopularFragment()
    }

    override fun onClick(any: Any, view: View) {
        if (any is MoviePopularModel) {
            goToDetail(any, view)
        }
    }

    private fun goToDetail(any: Any, view: View) {
        val popularModel = getMoviePopularModel(any)
        val transitionAnimation = getTransitionAnimation(view)
        val intent = Intent(context, MovieDetailActivity::class.java).apply {
            putExtra(MovieDetailActivity.MOVIE_ID_KEY, popularModel.id)
        }
        startActivity(intent, transitionAnimation?.toBundle())
    }

    private fun getMoviePopularModel(any: Any): MoviePopularModel {
        return any as MoviePopularModel
    }

    private fun getTransitionAnimation(view: View): ActivityOptionsCompat? {
        val elementName = getString(R.string.poster_image_view)
        return activity?.let { parentActivity ->
            ActivityOptionsCompat.makeSceneTransitionAnimation(parentActivity, view, elementName)
        }
    }
}