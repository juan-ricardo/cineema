package pe.com.cineema.popular.domain.repository

import pe.com.cineema.popular.domain.model.MoviePopularModel

interface MoviePopularRepository {
    suspend fun getMoviePopularModels(): List<MoviePopularModel>
}