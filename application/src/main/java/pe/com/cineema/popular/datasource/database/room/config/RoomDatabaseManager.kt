package pe.com.cineema.popular.datasource.database.room.config

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import pe.com.cineema.popular.datasource.database.room.dao.MoviePopularDao
import pe.com.cineema.popular.domain.entity.MoviePopularEntity

@Database(entities = [MoviePopularEntity::class], version = 1, exportSchema = false)
abstract class RoomDatabaseManager : RoomDatabase() {
    abstract fun moviePopularDao(): MoviePopularDao

    companion object {
        @Volatile
        private var INSTANCE: RoomDatabaseManager? = null
        fun getDatabase(context: Context): RoomDatabaseManager {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RoomDatabaseManager::class.java,
                    "the_movie_database"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}