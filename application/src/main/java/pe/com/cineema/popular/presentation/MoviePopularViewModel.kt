package pe.com.cineema.popular.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import pe.com.cineema.application.common.ui.GET_POPULAR_EXCEPTION
import pe.com.cineema.base.component.generic.Resource
import pe.com.cineema.popular.domain.repository.MoviePopularRepository
import javax.inject.Inject

@HiltViewModel
class MoviePopularViewModel @Inject constructor(
    private val popularRepository: MoviePopularRepository
) : ViewModel() {
    fun getMoviePopularModels() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = popularRepository.getMoviePopularModels()))
        } catch (exception: Exception) {
            val error = exception.message ?: GET_POPULAR_EXCEPTION
            emit(Resource.error(data = null, message = error))
        }
    }
}