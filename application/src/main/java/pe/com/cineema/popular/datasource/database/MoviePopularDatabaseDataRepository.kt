package pe.com.cineema.popular.datasource.database

import pe.com.cineema.popular.datasource.database.room.config.RoomDatabaseManager
import pe.com.cineema.popular.domain.mapper.MoviePopularMapper
import pe.com.cineema.popular.domain.model.MoviePopularModel
import javax.inject.Inject

class MoviePopularDatabaseDataRepository @Inject constructor(
    private val roomDatabaseManager: RoomDatabaseManager,
    private val moviePopularMapper: MoviePopularMapper
) {
    fun getPopularModels(): List<MoviePopularModel> {
        val moviePopularEntities = roomDatabaseManager.moviePopularDao().getAll()
        return moviePopularMapper.transform(moviePopularEntities)
    }

    fun savePopularModels(popularModels: List<MoviePopularModel>) {
        popularModels.map { moviePopularModel ->
            moviePopularMapper.transform(moviePopularModel)
        }.forEach { moviePopularEntity ->
            roomDatabaseManager.moviePopularDao().insert(moviePopularEntity)
        }
    }
}