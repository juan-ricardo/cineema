package pe.com.cineema.top.rated.datasource.network

import pe.com.cineema.top.rated.datasource.service.MovieTopRatedApiService
import pe.com.cineema.top.rated.domain.mapper.MovieTopRatedMapper
import pe.com.cineema.top.rated.domain.model.MovieTopRatedModel
import pe.com.cineema.top.rated.domain.repository.MovieTopRatedRepository
import javax.inject.Inject

class MovieTopRatedNetworkDataRepository @Inject constructor(
    private val movieTopRatedApiService: MovieTopRatedApiService,
    private val movieTopRatedMapper: MovieTopRatedMapper
) : MovieTopRatedRepository {
    override suspend fun getMovieTopRatedModels(): List<MovieTopRatedModel> {
        val response = movieTopRatedApiService.getMovieTopRatedResponse()
        return movieTopRatedMapper.transform(response)
    }
}