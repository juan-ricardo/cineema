package pe.com.cineema.top.rated.datasource.service

import pe.com.cineema.top.rated.domain.response.MovieTopRatedResponse
import retrofit2.http.GET

interface MovieTopRatedApiService {
    @GET("movie/top_rated")
    suspend fun getMovieTopRatedResponse(): MovieTopRatedResponse
}