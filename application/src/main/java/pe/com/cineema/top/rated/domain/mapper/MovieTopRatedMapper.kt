package pe.com.cineema.top.rated.domain.mapper

import pe.com.cineema.dashboard.domain.response.ResultResponse
import pe.com.cineema.top.rated.domain.model.MovieTopRatedModel
import pe.com.cineema.top.rated.domain.response.MovieTopRatedResponse
import javax.inject.Inject

class MovieTopRatedMapper @Inject constructor() {
    fun transform(topRatedResponse: MovieTopRatedResponse?): List<MovieTopRatedModel> {

        val results = topRatedResponse?.results
        val topRatedModels = results?.map { transformResponseToModel(it) }
        return topRatedModels ?: emptyList()
    }

    private fun transformResponseToModel(resultResponse: ResultResponse): MovieTopRatedModel {
        val adult = resultResponse.adult
        val backdropPath = resultResponse.backdropPath
        val id = resultResponse.id
        val originalLanguage = resultResponse.originalLanguage
        val originalTitle = resultResponse.originalTitle
        val overview = resultResponse.overview
        val posterPath = resultResponse.posterPath
        val releaseDate = resultResponse.releaseDate
        val title = resultResponse.title
        val video = resultResponse.video
        val voteAverage = resultResponse.voteAverage
        val voteCount = resultResponse.voteCount

        return MovieTopRatedModel(
            adult = adult,
            backdropPath = backdropPath,
            id = id,
            originalLanguage = originalLanguage,
            originalTitle = originalTitle,
            overview = overview,
            posterPath = posterPath,
            releaseDate = releaseDate,
            title = title,
            video = video,
            voteAverage = voteAverage,
            voteCount = voteCount
        )
    }
}

