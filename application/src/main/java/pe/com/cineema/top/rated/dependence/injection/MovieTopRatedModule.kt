package pe.com.cineema.top.rated.dependence.injection

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pe.com.cineema.top.rated.datasource.network.MovieTopRatedNetworkDataRepository
import pe.com.cineema.top.rated.datasource.service.MovieTopRatedApiService
import pe.com.cineema.top.rated.domain.mapper.MovieTopRatedMapper
import pe.com.cineema.top.rated.domain.repository.MovieTopRatedRepository
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MovieTopRatedModule {

    @Singleton
    @Provides
    fun topRatedApiServiceProvide(retrofit: Retrofit): MovieTopRatedApiService {
        return retrofit.create(MovieTopRatedApiService::class.java)
    }

    @Singleton
    @Provides
    fun movieTopRatedRepositoryProvide(
        topRatedApiService: MovieTopRatedApiService,
        topRatedMapper: MovieTopRatedMapper
    ): MovieTopRatedRepository {
        return MovieTopRatedNetworkDataRepository(topRatedApiService, topRatedMapper)
    }
}