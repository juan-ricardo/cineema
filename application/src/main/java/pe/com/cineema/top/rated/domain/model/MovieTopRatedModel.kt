package pe.com.cineema.top.rated.domain.model

import pe.com.cineema.base.component.util.EMPTY

data class MovieTopRatedModel(
    val adult: Boolean? = false,
    val backdropPath: String? = EMPTY,
    val id: Int? = 0,
    val originalLanguage: String? = EMPTY,
    val originalTitle: String? = EMPTY,
    val overview: String? = EMPTY,
    val posterPath: String? = EMPTY,
    val releaseDate: String? = EMPTY,
    val title: String? = EMPTY,
    val video: Boolean? = false,
    val voteAverage: Float? = 0.0f,
    val voteCount: Int? = 0,
)