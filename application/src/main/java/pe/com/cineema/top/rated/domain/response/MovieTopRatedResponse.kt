package pe.com.cineema.top.rated.domain.response

import com.google.gson.annotations.SerializedName
import pe.com.cineema.dashboard.domain.response.ResultResponse

class MovieTopRatedResponse(
    @SerializedName("page")
    val page: Int? = null,

    @SerializedName("results")
    val results: List<ResultResponse>? = null,

    @SerializedName("total_pages")
    val totalPages: Int? = null,

    @SerializedName("total_results")
    val totalResults: Int? = null
)