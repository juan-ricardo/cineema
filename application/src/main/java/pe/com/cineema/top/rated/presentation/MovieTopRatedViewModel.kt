package pe.com.cineema.top.rated.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import pe.com.cineema.application.common.ui.GET_TOP_RATED_EXCEPTION
import pe.com.cineema.base.component.generic.Resource
import pe.com.cineema.top.rated.domain.repository.MovieTopRatedRepository
import javax.inject.Inject

@HiltViewModel
class MovieTopRatedViewModel @Inject constructor(
    private val movieTopRatedRepository: MovieTopRatedRepository
) : ViewModel() {

    fun getMovieTopRatedModels() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = movieTopRatedRepository.getMovieTopRatedModels()))
        } catch (exception: Exception) {
            val error = exception.message ?: GET_TOP_RATED_EXCEPTION
            emit(Resource.error(data = null, message = error))
        }
    }
}