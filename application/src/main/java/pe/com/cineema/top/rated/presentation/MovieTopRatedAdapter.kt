package pe.com.cineema.top.rated.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import pe.com.cineema.R
import pe.com.cineema.application.common.util.IMAGE_URL_BASE
import pe.com.cineema.application.common.listener.ItemOnClickListener
import pe.com.cineema.databinding.TopRatedCardItemBinding
import pe.com.cineema.top.rated.domain.model.MovieTopRatedModel

class MovieTopRatedAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var topRatedModels = mutableListOf<MovieTopRatedModel>()
    private var filterTopRatedModels = mutableListOf<MovieTopRatedModel>()
    private var itemOnClickListener: ItemOnClickListener? = null

    fun setItems(popularModels: MutableList<MovieTopRatedModel>) {
        this.topRatedModels.addAll(popularModels)
        this.filterTopRatedModels.addAll(popularModels)
    }

    fun setItemOnClickListener(itemOnClickListener: ItemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val topRatedCardItemBinding = TopRatedCardItemBinding.inflate(layoutInflater)
        return TopRatedViewHolder(topRatedCardItemBinding, itemOnClickListener)
    }

    inner class TopRatedViewHolder(
        private val topRatedCardItemBinding: TopRatedCardItemBinding,
        private val itemOnClickListener: ItemOnClickListener?
    ) : RecyclerView.ViewHolder(topRatedCardItemBinding.root) {
        fun onBind(topRatedModel: MovieTopRatedModel) {
            topRatedCardItemBinding.root.setOnClickListener {
                itemOnClickListener?.onClick(topRatedModel, topRatedCardItemBinding.posterImageView)
            }
            topRatedCardItemBinding.titleTextView.text = topRatedModel.title
            setupPoster(topRatedModel)
        }

        private fun setupPoster(topRatedModel: MovieTopRatedModel) {
            val url = IMAGE_URL_BASE.plus(topRatedModel.posterPath)
            val imageView = topRatedCardItemBinding.posterImageView
            Picasso.get().load(url)
                .placeholder(R.drawable.movie_placeholder)
                .error(R.drawable.movie_placeholder)
                .into(imageView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = topRatedModels[position]
        if (holder is TopRatedViewHolder) {
            holder.onBind(item)
        }
    }

    override fun getItemCount() = topRatedModels.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val filterResults = FilterResults()
                if (charSequence.isNotEmpty()) {
                    val query = charSequence.toString()
                    val filters: MutableList<MovieTopRatedModel> = mutableListOf()
                    for (item in filterTopRatedModels) {
                        val title = item.title?.contains(query, true)
                        val originalTitle = item.originalTitle?.contains(query, true)
                        if (title == true || originalTitle == true) {
                            filters.add(item)
                        }
                    }
                    filterResults.count = filters.size
                    filterResults.values = filters
                } else {
                    filterResults.count = filterTopRatedModels.size
                    filterResults.values = filterTopRatedModels
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                topRatedModels = results.values as MutableList<MovieTopRatedModel>
                notifyDataSetChanged()
            }
        }
    }
}