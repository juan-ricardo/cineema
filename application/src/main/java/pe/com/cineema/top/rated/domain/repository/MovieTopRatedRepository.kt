package pe.com.cineema.top.rated.domain.repository

import pe.com.cineema.top.rated.domain.model.MovieTopRatedModel

interface MovieTopRatedRepository {
    suspend fun getMovieTopRatedModels(): List<MovieTopRatedModel>
}