package pe.com.cineema.top.rated.presentation

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pe.com.cineema.R
import pe.com.cineema.application.common.listener.ItemOnClickListener
import pe.com.cineema.application.common.ui.SpacesItemDecoration
import pe.com.cineema.base.component.generic.Resource
import pe.com.cineema.base.component.generic.Status
import pe.com.cineema.base.component.ui.BaseFragment
import pe.com.cineema.databinding.FragmentMovieTopRatedBinding
import pe.com.cineema.detail.presentation.MovieDetailActivity
import pe.com.cineema.top.rated.domain.model.MovieTopRatedModel

@AndroidEntryPoint
class MovieTopRatedFragment : BaseFragment<FragmentMovieTopRatedBinding>(), ItemOnClickListener {

    private val movieTopRatedViewModel: MovieTopRatedViewModel by viewModels()
    private val movieTopRatedAdapter: MovieTopRatedAdapter by lazy { MovieTopRatedAdapter() }

    companion object {
        fun newInstance() = MovieTopRatedFragment()
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToRoot: Boolean?
    ): FragmentMovieTopRatedBinding {
        return FragmentMovieTopRatedBinding.inflate(layoutInflater)
    }

    override fun initConfig() {
        setupObservers()
        setupPopularRecyclerView()
    }

    private fun setupPopularRecyclerView() {
        val orientacion = StaggeredGridLayoutManager.VERTICAL
        val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, orientacion)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.cineema_margin_4)
        val spacesItemDecoration = SpacesItemDecoration(spacingInPixels)
        binding.topRatedRecyclerView.layoutManager = staggeredGridLayoutManager
        binding.topRatedRecyclerView.addItemDecoration(spacesItemDecoration)
    }

    private fun setupObservers() {
        getMovieTopRatedModels()
    }

    private fun getMovieTopRatedModels() {
        val movieTopRatedModels = movieTopRatedViewModel.getMovieTopRatedModels()
        movieTopRatedModels.observe(this, { values ->
            values?.let { resource -> statusForMoviePopularModels(resource) }
        })
    }

    private fun statusForMoviePopularModels(resource: Resource<List<MovieTopRatedModel>>) {
        when (resource.status) {
            Status.SUCCESS -> { showMovieTopRatedModels(resource) }
            Status.ERROR -> { showError() }
            Status.LOADING -> { showProgressBar() }
        }
    }

    private fun showMovieTopRatedModels(resource: Resource<List<MovieTopRatedModel>>) {
        setupMovieTopRatedAdapter(resource)
        hideProgressBar()
        showTopRatedRecyclerView()
    }

    private fun setupMovieTopRatedAdapter(resource: Resource<List<MovieTopRatedModel>>) {
        val movieTopRatedModels = getMovieTopRatedModels(resource)
        if (movieTopRatedModels.isEmpty()) {
            showErrorOccurred()
        } else {
            setupTopRatedAdapter(movieTopRatedModels)
            showTopRatedRecyclerView()
        }
        hideProgressBar()
    }

    private fun setupTopRatedAdapter(movieTopRatedModels: MutableList<MovieTopRatedModel> = mutableListOf()) {
        movieTopRatedAdapter.setItems(movieTopRatedModels)
        movieTopRatedAdapter.setItemOnClickListener(this)
        binding.topRatedRecyclerView.adapter = movieTopRatedAdapter
    }

    private fun getMovieTopRatedModels(resource: Resource<List<MovieTopRatedModel>>): MutableList<MovieTopRatedModel> {
        return resource.data?.toMutableList() ?: mutableListOf()
    }

    fun filterTopRatedMovies(query: String) {
        movieTopRatedAdapter.filter.filter(query)
    }

    private fun showError() {
        setupErrorOccurred()
        hideProgressBar()
        hideTopRatedRecyclerView()
        showErrorOccurred()
    }

    private fun setupErrorOccurred() {
        binding.errorOccurred.retryMaterialButton.setOnClickListener { retryErrorOccurred() }
    }

    private fun retryErrorOccurred() {
        hideErrorOccurred()
        showProgressBar()
        getMovieTopRatedModels()
    }

    private fun showErrorOccurred() {
        binding.errorOccurred.root.visibility = View.VISIBLE
    }

    private fun hideErrorOccurred() {
        binding.errorOccurred.root.visibility = View.GONE
    }

    private fun showTopRatedRecyclerView() {
        binding.topRatedRecyclerView.visibility = View.VISIBLE
    }

    private fun hideTopRatedRecyclerView() {
        binding.topRatedRecyclerView.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.topRatedProgressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.topRatedProgressBar.visibility = View.GONE
    }

    override fun onClick(any: Any, view: View) {
        if (any is MovieTopRatedModel) {
            goToDetail(any, view)
        }
    }

    private fun goToDetail(any: Any, view: View) {
        val movieTopRatedModel = getMovieTopRatedModel(any)
        val transitionAnimation = getTransitionAnimation(view)
        val intent = Intent(context, MovieDetailActivity::class.java).apply {
            putExtra(MovieDetailActivity.MOVIE_ID_KEY, movieTopRatedModel.id)
        }
        startActivity(intent, transitionAnimation?.toBundle())

    }

    private fun getMovieTopRatedModel(any: Any): MovieTopRatedModel {
        return any as MovieTopRatedModel
    }

    private fun getTransitionAnimation(view: View): ActivityOptionsCompat? {
        val elementName = getString(R.string.poster_image_view)
        return activity?.let { parentActivity ->
            ActivityOptionsCompat.makeSceneTransitionAnimation(parentActivity, view, elementName)
        }
    }
}