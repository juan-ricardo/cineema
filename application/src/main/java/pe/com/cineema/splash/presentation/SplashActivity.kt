package pe.com.cineema.splash.presentation

import android.content.Intent
import android.os.Handler
import android.os.Looper
import pe.com.cineema.base.component.ui.BaseActivity
import pe.com.cineema.databinding.ActivitySplashBinding
import pe.com.cineema.dashboard.presentation.DashboardActivity

class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    companion object {
        const val DELAY_MILLIS = 2500L
    }

    override fun getViewBinding() = ActivitySplashBinding.inflate(layoutInflater)

    override fun initConfig() {

    }

    override fun onResume() {
        super.onResume()
        Looper.myLooper()?.let { Handler(it).postDelayed(::goToDashboard, DELAY_MILLIS) }
    }

    private fun goToDashboard() {
        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
    }
}