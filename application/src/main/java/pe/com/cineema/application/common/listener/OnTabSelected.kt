package pe.com.cineema.application.common.listener

import com.google.android.material.tabs.TabLayout

interface OnTabSelectedListener {
    fun onTabSelected(tab: TabLayout.Tab?)
}