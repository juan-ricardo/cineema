package pe.com.cineema.application.network

import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TheMovieDbNetworkInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val response = chain.proceed(request)
        val cacheControl = response.header(CACHE_CONTROL_HEADER)
        return if (cacheControl == null
            || cacheControl.contains(NO_STORE)
            || cacheControl.contains(NO_CACHE)
            || cacheControl.contains(MUST_REVALIDATE)
            || cacheControl.contains(MAX_STALE)
        ) {
            val cacheControl: CacheControl = CacheControl.Builder()
                .maxStale(MAX_SCALE_VALUE, TimeUnit.DAYS)
                .build()
            request = request.newBuilder()
                .cacheControl(cacheControl)
                .build()
            chain.proceed(request)
        } else {
            response
        }
    }
}