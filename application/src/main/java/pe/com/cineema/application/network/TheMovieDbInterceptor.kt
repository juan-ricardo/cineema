package pe.com.cineema.application.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import pe.com.cineema.BuildConfig
import javax.inject.Inject

class TheMovieDbInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val httpUrl = request.url

        val url = httpUrl.newBuilder()
            .addQueryParameter(BuildConfig.API_KEY_TAG, BuildConfig.THEMOVIEDB_API_KEY)
            .addQueryParameter(BuildConfig.LANGUAGE_TAG, LAGUAGE_ES)
            .build()

        val requestBuilder = request.newBuilder()
            .url(url)

        val newRequest: Request = requestBuilder.build()
        return chain.proceed(newRequest)
    }
}