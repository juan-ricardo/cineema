package pe.com.cineema.application.common.listener

import android.view.View

interface ItemOnClickListener {
    fun onClick(any: Any, view: View)
}