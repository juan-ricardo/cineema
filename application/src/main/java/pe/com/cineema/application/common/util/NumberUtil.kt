package pe.com.cineema.application.common.util

const val ZERO = 0
const val ONE = 1
const val TWO = 2
const val ZERO_POSITION = 0