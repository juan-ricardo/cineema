package pe.com.cineema.application.network

import android.content.Context
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class CacheInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            return chain.proceed(chain.request())
        } catch (exception: Exception) {

        }

        val cacheControl = CacheControl.Builder()
            .onlyIfCached()
            .maxStale(MAX_SCALE_VALUE, TimeUnit.DAYS)
            .build()
        val request = chain.request().newBuilder().cacheControl(cacheControl).build()
        return chain.proceed(request)
    }
}