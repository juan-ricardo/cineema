package pe.com.cineema.application.common.ui

const val GET_POPULAR_EXCEPTION = " Get Popular Exception"
const val GET_TOP_RATED_EXCEPTION = " Get Top Rated Exception"
const val GET_DETAIL_EXCEPTION = " Get Detail Exception"
const val GET_VIDEOS_EXCEPTION = " Get Videos Exception"

const val SPACE = " "
const val SPACE_PLUS_BAR = " | "