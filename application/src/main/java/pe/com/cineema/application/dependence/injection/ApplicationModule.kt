package pe.com.cineema.application.dependence.injection

import android.app.Application
import com.chuckerteam.chucker.api.ChuckerInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pe.com.cineema.BuildConfig
import pe.com.cineema.application.common.util.TIME_OUT
import pe.com.cineema.application.network.*
import pe.com.cineema.base.component.security.EncryptedSharedPreferences
import pe.com.cineema.detail.datasource.service.MovieDetailApiService
import pe.com.cineema.detail.datasource.service.VideoApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    fun encryptedSharedPreferencesProvide(application: Application): EncryptedSharedPreferences {
        return EncryptedSharedPreferences(application.applicationContext)
    }

    @Provides
    fun httpLoggingInterceptorProvide(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
    }

    @Provides
    fun chuckerInterceptorProvide(application: Application): ChuckerInterceptor {
        return ChuckerInterceptor.Builder(application.applicationContext)
            .build()
    }

    @Provides
    fun provideHttpClient(
        cache: Cache,
        theMovieDbInterceptor: TheMovieDbInterceptor,
        chuckerInterceptor: ChuckerInterceptor,
        cacheInterceptor: CacheInterceptor,
        theMovieDbNetworkInterceptor: TheMovieDbNetworkInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .cache(cache)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(chuckerInterceptor)
            .addInterceptor(theMovieDbInterceptor)
            .addInterceptor(cacheInterceptor)
            .addNetworkInterceptor(theMovieDbNetworkInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun cacheProvide(application: Application): Cache {
        val parentPathName = application.applicationContext.cacheDir
        val httpCacheDirectory = File(parentPathName, CHILD_PATH_NAME)
        return Cache(httpCacheDirectory, MAX_SIZE.toLong())
    }

    @Singleton
    @Provides
    fun gsonConverterFactoryProvide(): GsonConverterFactory = GsonConverterFactory.create()

    @Singleton
    @Provides
    fun retrofitProvide(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.URL_BASE)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
    }

    @Singleton
    @Provides
    fun detailApiServiceProvide(retrofit: Retrofit): MovieDetailApiService {
        return retrofit.create(MovieDetailApiService::class.java)
    }

    @Singleton
    @Provides
    fun videoApiServiceProvide(retrofit: Retrofit): VideoApiService {
        return retrofit.create(VideoApiService::class.java)
    }
}