package pe.com.cineema.application.network

const val LAGUAGE_ES = "es-ES"

const val CHILD_PATH_NAME = "offlineCache"
const val MAX_SIZE = 10 * 1024 * 1024

const val NO_STORE = "no-store"
const val NO_CACHE = "no-cache"
const val MUST_REVALIDATE = "must-revalidate"
const val MAX_STALE = "max-stale=0"
const val MAX_SCALE_VALUE = 1
const val CACHE_CONTROL_HEADER = "Cache-Control"