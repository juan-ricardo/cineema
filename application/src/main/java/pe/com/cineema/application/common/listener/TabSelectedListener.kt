package pe.com.cineema.application.common.listener

import com.google.android.material.tabs.TabLayout

class TabSelectedListener(
    private val onTabSelectedListener: OnTabSelectedListener
) : TabLayout.OnTabSelectedListener {

    override fun onTabSelected(tab: TabLayout.Tab?) {
        onTabSelectedListener.onTabSelected(tab)
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
        // No required
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {
        // No required
    }
}