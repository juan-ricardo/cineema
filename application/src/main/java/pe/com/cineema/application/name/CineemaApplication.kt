package pe.com.cineema.application.name

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CineemaApplication : Application()
