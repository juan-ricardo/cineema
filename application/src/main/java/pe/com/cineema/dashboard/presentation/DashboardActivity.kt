package pe.com.cineema.dashboard.presentation

import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import pe.com.cineema.R
import pe.com.cineema.application.common.listener.OnTabSelectedListener
import pe.com.cineema.application.common.listener.TabSelectedListener
import pe.com.cineema.application.common.util.ZERO_POSITION
import pe.com.cineema.base.component.security.EncryptedSharedPreferences
import pe.com.cineema.base.component.ui.BaseActivity
import pe.com.cineema.base.component.util.EMPTY
import pe.com.cineema.databinding.ActivityDashboardBinding
import javax.inject.Inject

@AndroidEntryPoint
class DashboardActivity : BaseActivity<ActivityDashboardBinding>() {

    @Inject
    lateinit var encryptedSharedPreferences: EncryptedSharedPreferences

    lateinit var searchView: SearchView
    private var weAreInThePopularTab = true

    private val movieFragmentStateAdapter: MovieFragmentStateAdapter by lazy {
        MovieFragmentStateAdapter(this)
    }

    override fun getViewBinding() = ActivityDashboardBinding.inflate(layoutInflater)

    override fun initConfig() {
        setupViews()
    }

    private fun setupViews() {
        setupToolbar()
        setupMovieFragmentStateAdapter()
        setupTabLayout()
    }

    private fun setupToolbar() {
        val toolbar = binding.appBarLayout.toolbar
        setSupportActionBar(toolbar)
    }

    private fun setupMovieFragmentStateAdapter() {
        binding.viewPager.adapter = movieFragmentStateAdapter
    }

    private fun setupTabLayout() {
        val tabLayout = binding.appBarLayout.tabLayout
        //tabLayout.addOnTabSelectedListener(TabSelectedListener(onTabSelectedListener()))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                weAreInThePopularTab = tab?.position == ZERO_POSITION
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // No required
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // No required
            }

        })
        val viewPager = binding.viewPager
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            setupTabLayoutTitle(tab, position)
        }.attach()
    }

    private fun onTabSelectedListener() = object : OnTabSelectedListener {
        override fun onTabSelected(tab: TabLayout.Tab?) {
            weAreInThePopularTab = tab?.position == ZERO_POSITION
        }
    }

    private fun setupTabLayoutTitle(tabLayout: TabLayout.Tab, position: Int) {
        if (position == ZERO_POSITION) {
            tabLayout.text = getString(R.string.popular)
        } else {
            tabLayout.text = getString(R.string.top_rated)
        }
    }

    private fun setupSearchView(menu: Menu) {
        val searchItemMenu = menu.findItem(R.id.searchItem)
        searchView = searchItemMenu.actionView as SearchView
        searchView.queryHint = getString(R.string.search)
        searchView.setOnQueryTextListener(setupOnQueryTextListener())
    }

    private fun setupOnQueryTextListener() = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            filterSetting(newText)
            return true
        }
    }

    private fun filterSetting(newText: String?) {
        if (weAreInThePopularTab) {
            movieFragmentStateAdapter.getPopularFragment()
                .filterPopularMovies(newText ?: EMPTY)
        } else {
            movieFragmentStateAdapter.getTopRatedFragment()
                .filterTopRatedMovies(newText ?: EMPTY)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        setupSearchView(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.searchItem -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}