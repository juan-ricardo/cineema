package pe.com.cineema.dashboard.presentation

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import pe.com.cineema.popular.presentation.MoviePopularFragment
import pe.com.cineema.top.rated.presentation.MovieTopRatedFragment

class MovieFragmentStateAdapter(@NonNull fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    private val popularFragment = MoviePopularFragment.newInstance()
    private val topRatedFragment = MovieTopRatedFragment.newInstance()

    override fun getItemCount() = 2

    override fun createFragment(position: Int): Fragment {
        return if (position == 0) {
            popularFragment
        } else {
            topRatedFragment
        }
    }

    fun getPopularFragment() = popularFragment
    fun getTopRatedFragment() = topRatedFragment
}