package pe.com.cineema.detail.datasource.network

import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import pe.com.cineema.detail.datasource.service.MovieDetailApiService
import pe.com.cineema.detail.domain.mapper.MovieDetailMapper
import pe.com.cineema.detail.domain.model.MovieDetailModel
import pe.com.cineema.detail.domain.response.MovieDetailResponse

@RunWith(MockitoJUnitRunner::class)
class DetailNetworkDataRepositoryTest {

    @Mock
    lateinit var movieDetailApiService: MovieDetailApiService

    @Mock
    lateinit var movieDetailMapper: MovieDetailMapper

    private lateinit var movieDetailNetworkDataRepository: MovieDetailNetworkDataRepository

    @Before
    fun setup() {
        movieDetailNetworkDataRepository =
            MovieDetailNetworkDataRepository(movieDetailApiService, movieDetailMapper)
    }

    @Test
    fun `should return null when response is null`() = runTest {
        val response = MovieDetailResponse()

        Mockito.`when`(movieDetailApiService.getMovieDetailResponse(8765))
            .thenReturn(response)

        val expected = movieDetailNetworkDataRepository.getMovieDetail(8765)
        val actual = MovieDetailModel()
        Assert.assertEquals(expected, null)

        Mockito.verify(
            movieDetailApiService,
            Mockito.times(1)
        ).getMovieDetailResponse(8765)
    }
}