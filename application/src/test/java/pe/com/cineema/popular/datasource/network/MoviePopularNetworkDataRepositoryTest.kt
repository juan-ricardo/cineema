package pe.com.cineema.popular.datasource.network

import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import pe.com.cineema.popular.datasource.network.service.MoviePopularApiService
import pe.com.cineema.popular.domain.mapper.MoviePopularMapper
import pe.com.cineema.popular.domain.response.MoviePopularResponse

@RunWith(MockitoJUnitRunner::class)
class MoviePopularNetworkDataRepositoryTest {

    @Mock
    lateinit var moviePopularApiService: MoviePopularApiService

    @Mock
    lateinit var moviePopularMapper: MoviePopularMapper

    private lateinit var popularNetworkDataRepository: MoviePopularNetworkDataRepository

    @Before
    fun setup() {
        popularNetworkDataRepository =
            MoviePopularNetworkDataRepository(moviePopularApiService, moviePopularMapper)
    }

    @Test
    fun `should return empty list when response is null`() = runTest {
        val response = MoviePopularResponse(
            null,
            null
        )

        Mockito.`when`(moviePopularApiService.getPopularResponse())
            .thenReturn(response)

        Assert.assertTrue(popularNetworkDataRepository.getPopularModels().isEmpty())

        Mockito.verify(
            moviePopularApiService,
            Mockito.times(1)
        ).getPopularResponse()
    }
}