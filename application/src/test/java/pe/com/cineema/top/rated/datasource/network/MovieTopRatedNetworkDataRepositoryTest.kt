package pe.com.cineema.top.rated.datasource.network

import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import pe.com.cineema.top.rated.datasource.service.MovieTopRatedApiService
import pe.com.cineema.top.rated.domain.mapper.MovieTopRatedMapper
import pe.com.cineema.top.rated.domain.response.MovieTopRatedResponse

@RunWith(MockitoJUnitRunner::class)
class MovieTopRatedNetworkDataRepositoryTest {
    @Mock
    lateinit var movieTopRatedApiService: MovieTopRatedApiService

    @Mock
    lateinit var movieTopRatedMapper: MovieTopRatedMapper

    private lateinit var movieTopRatedNetworkDataRepository: MovieTopRatedNetworkDataRepository

    @Before
    fun setup() {
        movieTopRatedNetworkDataRepository =
            MovieTopRatedNetworkDataRepository(movieTopRatedApiService, movieTopRatedMapper)
    }

    @Test
    fun `should return empty list when response is null`() = runTest {
        val response = MovieTopRatedResponse(null, null, null, null)

        Mockito.`when`(movieTopRatedApiService.getMovieTopRatedResponse())
            .thenReturn(response)

        Assert.assertTrue(movieTopRatedNetworkDataRepository.getMovieTopRatedModels().isEmpty())

        Mockito.verify(
            movieTopRatedApiService,
            Mockito.times(1)
        ).getMovieTopRatedResponse()
    }
}