# Cineema

* ¿Qué es?: Es una app que funciona desde Android 7, hasta Android 12.

* Referencias:

    * https://github.com/Kotlin/kotlinx.coroutines/blob/master/kotlinx-coroutines-test/MIGRATION.md
  
    * https://www.raywenderlich.com/books/real-world-android-by-tutorials/v1.0/chapters/4-data-layer-network
  
    * https://www.raywenderlich.com/books/real-world-android-by-tutorials/v1.0/chapters/3-domain-layer
  
    * https://www.raywenderlich.com/books/real-world-android-by-tutorials/v1.0/chapters/5-data-layer-caching
      
    * https://medium.com/androiddevelopers/easy-coroutines-in-android-viewmodelscope-25bffb605471
  
    * https://www.journaldev.com/23297/android-retrofit-okhttp-offline-caching
  
    * https://www.raywenderlich.com/books/saving-data-on-android/v1.0/chapters/5-room-architecture

    * https://www.geeksforgeeks.org/kotlin-flow-in-android-with-example/

    * https://developer.android.com/topic/libraries/architecture/coroutines
  
    * https://waynestalk.com/en/category/android-en/