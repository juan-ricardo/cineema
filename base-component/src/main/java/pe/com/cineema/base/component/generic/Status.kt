package pe.com.cineema.base.component.generic

enum class Status {
    SUCCESS, ERROR, LOADING
}