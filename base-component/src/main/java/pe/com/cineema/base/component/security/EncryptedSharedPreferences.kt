package pe.com.cineema.base.component.security

import android.content.Context
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import org.jetbrains.annotations.NotNull

private const val CINEEMA_SHARED_PREFERENCES = "CINEEMA_SHARED_PREFERENCES"

class EncryptedSharedPreferences constructor(context: Context) {

    private val sharedPreferences by lazy {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        val keyEncryptionScheme = EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV
        val valueEncryptionScheme = EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        EncryptedSharedPreferences.create(
            CINEEMA_SHARED_PREFERENCES,
            masterKeyAlias,
            context,
            keyEncryptionScheme,
            valueEncryptionScheme
        )
    }

    fun getString(@NotNull key: String, @NotNull defValue: String): String? {
        return sharedPreferences.getString(key, defValue)
    }

    fun putString(@NotNull key: String, @NotNull value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }
}