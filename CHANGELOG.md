Change Log
==========

Version 1.0.1 *(2022-01-01)*
----------------------------

 * Nuevo: Migración a Flow!
 * Integración de nuevos casos de peliculas (Estrenos, lo más reciente, etc)
 * Integración de Crashlytics


Version 1.0.0 *(2022-01-01)*
----------------------------

 * Feature: Dashboard
 * Feature: Movies `Populars`
 * Feature: Movies `Top Rated`
 

Version 1.0.0 *(2021-12-20)*
----------------------------

Initial release.
